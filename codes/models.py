from django.db import models
from django.contrib.auth import get_user_model
from students.models import Student
from random import randrange


def generate_unique_id():
    while True:
        num = randrange(10000, 30000)
        if Code.objects.filter(code_id=num).exists():
            continue
        else:
            return num


class Code(models.Model):

    code_id = models.PositiveSmallIntegerField(default=generate_unique_id)
    lecture = models.ForeignKey('lectures.Lecture', on_delete=models.CASCADE, related_name="codes")
    in_use = models.BooleanField(default=False)
    student = models.ForeignKey(Student, null=True, blank=True, on_delete=models.CASCADE, related_name="codes")
    checked_by = models.ForeignKey(get_user_model(),
                                   null=True, blank=True,on_delete=models.SET_NULL, related_name="checked_codes")

    class Meta:
        unique_together = (('code_id', 'lecture',),
                           ('lecture', 'student'))

    def __str__(self):
        return '%s %s' % (self.lecture, self.pk)
