from django.db import models
from django.db.models.signals import post_save, post_init, pre_save, m2m_changed
from django.dispatch import receiver
from codes.models import Code
from uuid import uuid4
import string
import random


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def uuid_to_str():
    return str(uuid4())


class Lecture(models.Model):
    title = models.CharField(max_length=50)
    url = models.CharField(max_length=50, unique=True, default=id_generator)
    is_active = models.BooleanField(default=False)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        # update random url each time instance is saved
        self.url = id_generator()

        # when a lecture is marked as active, mark all others as non-active
        if self.is_active:
            for lecture in Lecture.objects.exclude(pk=self.pk):
                lecture.is_active = False
                lecture.save()
        super(Lecture, self).save()

    def __str__(self):
        return '%s %s' % (self.title, self.pk)


@receiver(post_save, sender=Lecture)
def populate_codes(sender, instance, created, **kwargs):
    if created:
        for i in range(10):
            Code.objects.create(lecture=instance)

