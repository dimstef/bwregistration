from django.contrib import admin
from .models import Lecture
from codes.models import Code


class CodeInline(admin.TabularInline):
    model = Code


class LectureAdmin(admin.ModelAdmin):
    inlines = [
        CodeInline,
    ]


admin.site.register(Lecture)  # ,LectureAdmin