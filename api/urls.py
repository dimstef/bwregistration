from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers, serializers, viewsets
from rest_framework_swagger.views import get_swagger_view
from .views import UsersViewSet,UserViewSet, CodeViewSet, UploadStudents, LectureViewSet, LectureCodes,\
    ActiveLectureViewSet, AllCodesViewSet

schema_view = get_swagger_view(title='Pastebin API')

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UsersViewSet)
router.register(r'user', UserViewSet)
router.register(r'code', CodeViewSet)
router.register(r'all_codes', AllCodesViewSet)
router.register(r'active_lecture', ActiveLectureViewSet)
router.register(r'lectures', LectureViewSet)
router.register('lectures/(?P<id>.+)/codes', LectureCodes),

#router.register('upload_students/', UploadStudents.as_view()),

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^$', schema_view),
    url(r'^', include(router.urls)),
    path('upload_students/', UploadStudents.as_view()),
    url(r'^auth/', include('rest_auth.urls'))
]