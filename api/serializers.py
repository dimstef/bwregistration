from rest_framework import routers, serializers, viewsets
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework import status
from codes.models import Code
from lectures.models import Lecture
from students.models import Student


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff', 'is_superuser']


class StudentSerializer(serializers.ModelSerializer):
    full_name = serializers.ReadOnlyField()

    class Meta:
        model = Student
        fields = ['id', 'student_id', 'name', 'name', 'surname', 'email', 'full_name']


class CodeSerializer(serializers.ModelSerializer):
    student = StudentSerializer(required=False)

    class Meta:
        model = Code
        fields = ['id', 'code_id', 'student', 'checked_by', 'lecture', 'in_use']
        read_only_fields = ['id', 'lecture']

    def update(self, instance, validated_data):

        # student_id may not exist in data
        try:
            request = self.context['request']
            student_id = request.data['student_id']
            student = Student.objects.get(student_id=student_id)
            instance.student = student
            instance.checked_by = request.user
        except Exception:
            pass
        return super(CodeSerializer, self).update(instance, validated_data)


class LectureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lecture
        fields = '__all__'


class LectureUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lecture
        fields = ['is_active']
