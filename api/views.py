from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets, mixins, permissions, views
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser,MultiPartParser
from rest_framework import status
from rest_framework.response import Response
from codes.models import Code
from lectures.models import Lecture
from students.models import Student
from . import serializers
import csv
import codecs


class UsersViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class UserViewSet(viewsets.GenericViewSet,
                  mixins.ListModelMixin):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer

    def get_queryset(self):
        return User.objects.filter(pk=self.request.user.pk)


class CodeViewSet(viewsets.ModelViewSet):

    queryset = Code.objects.all()
    serializer_class = serializers.CodeSerializer

    def get_permissions(self):
        if self.action == 'list':
            permission_classes = [permissions.AllowAny]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]

    def list(self, request, *args, **kwargs):
        active_lecture = Lecture.objects.filter(is_active=True)
        if active_lecture.exists():
            code = Code.objects.filter(in_use=False, lecture=active_lecture.first()).first()
            code.in_use = True
            code.save()
            serializer = self.serializer_class(code)
        else:
            serializer = self.serializer_class(Code.objects.none())
        return Response(serializer.data)

    def get_serializer_context(self):
        return {
            'request': self.request,
        }

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance.in_use:
            return Response({'code': 'Code is not yet used'}, status=status.HTTP_400_BAD_REQUEST)
        if not instance.lecture.is_active:
            return Response({'code': 'Code does not belong to the active lecture'}, status=status.HTTP_400_BAD_REQUEST)
        return super(CodeViewSet, self).update(request, *args, **kwargs)


class AllCodesViewSet(viewsets.GenericViewSet,
                      mixins.ListModelMixin):
    queryset = Code.objects.all()
    serializer_class = serializers.CodeSerializer
    permission_classes = (permissions.IsAdminUser,)


class UploadStudents(views.APIView):
    permission_classes = (permissions.AllowAny,)
    parser_classes = (MultiPartParser, FileUploadParser)

    def post(self, request, format=None):
        file_obj = request.FILES['file']
        csv_reader = csv.DictReader(codecs.iterdecode(file_obj, 'utf-8-sig'))
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1

            student = Student.objects.update_or_create(student_id=row['StudentID'], name=row['Name'],
                                                       surname=row['Surname'], email=row['email'])

            line_count += 1
        return Response(status=204)


class LectureViewSet(viewsets.GenericViewSet,
                     mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.CreateModelMixin):

    queryset = Lecture.objects.all()
    permission_classes = (permissions.AllowAny,)

    def get_serializer_class(self):
        if self.action == 'update':
            return serializers.LectureUpdateSerializer

        return serializers.LectureSerializer


class ActiveLectureViewSet(viewsets.GenericViewSet,
                           mixins.ListModelMixin):

    queryset = Lecture.objects.all()
    serializer_class = serializers.LectureSerializer
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        return Lecture.objects.filter(is_active=True)


class LectureCodes(viewsets.GenericViewSet,
                   mixins.ListModelMixin):

    queryset = Code.objects.none()
    serializer_class = serializers.CodeSerializer
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        lecture = Lecture.objects.get(pk=self.kwargs['id'])
        return lecture.codes.all()

    def list(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.get_queryset(), many=True)
        return Response(serializer.data)


def generate_codes(instance, count=100):
    for i in range(count):
        Code.objects.create(lecture=instance)
