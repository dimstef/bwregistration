import React, {useEffect,useState,useRef,useContext} from "react"
import {Field,Form} from "react-final-form";
import {css} from "@emotion/core"
import history from "./history"
import {getCookie} from "./getCookie"
import {UserContext} from "../container/Context";
import { FORM_ERROR } from 'final-form'
import axios from "axios";

const formCss = () =>css({
    display:'flex',
    flexFlow:'column',
    alignItems:'center',
    width:'50%',
    '@media (max-device-width:767px)':{
        width:'80%'
    }
})

export function Login({handleResponse=()=>{}}){
    const userContext = useContext(UserContext);

    async function onSubmit(values){
        let errors = {};
        
        try{
            let config = {
                headers:{
                    'X-CSRFToken':getCookie('csrftoken')
                }
            }
            let data = {
                ...values,
                username:''
            }

            let response = await axios.post('/api/auth/login/',data,config);
            try{
                userContext.isAuth = response.data.token || false;
                userContext.user = response.data.user || null
                localStorage.setItem('token',userContext.isAuth);
                handleResponse(response);
            }catch(e){

            }

        }catch(error){
            if(error.response.data.non_field_errors){
                //errors.password = error.response.data.non_field_errors[0];
                return {[FORM_ERROR]: error.response.data.non_field_errors[0]}
            }
        }
        return errors;
    }

    return(
        <Form onSubmit={onSubmit} render={({handleSubmit,submitting,submitSucceeded,submitFailed,pristine,
        invalid,errors,submitError})=>{
            return(
                <form style={{margin:'0.7em'}} onSubmit={handleSubmit} css={formCss}>
                    <p>Login</p>
                    <Field name="email" type="email" required>{({input,meta})=>{
                        return <div>
                            <input {...input} placeholder='Email' 
                            style={{height:30,border:'1px solid #c3c3c3',borderRadius:24,width:'100%',padding:5}}/>
                            {(meta.error || meta.submitError) && meta.touched && <p>{meta.error || meta.submitError}</p>}
                        </div>
                    }}
                    </Field>
                    <Field name="password" type="password" required>{({input,meta})=>(
                        <div>
                            <input {...input} placeholder='Password' 
                            style={{height:30,border:'1px solid #c3c3c3',borderRadius:24,width:'100%',padding:5}}/>
                            {(meta.error || meta.submitError) && meta.touched && 
                            <p css={{color:'#f44336',fontSize:'1rem',fontWeight:'bold'}}>{meta.error || meta.submitError}</p>}
                        </div>
                    )}
                    </Field>
                    {submitSucceeded?<p>Successfully logged in</p>:null}
                    {submitError?<p css={{color:'#f44336',fontSize:'1rem',fontWeight:'bold'}}>{submitError}</p>:null}
                    <div css={{margin:20}}>
                        <Save submitting={submitting} submitSucceeded={submitSucceeded} pristine={pristine} invalid={invalid}/>

                    </div>
                </form>
            )
        }}></Form>
    )
}

function Save({submitting,pristine,invalid}){
    return(
        <button type="submit" disabled={submitting || pristine}>
            Login
        </button>
    )
}