import React, {useEffect,useState,useRef} from "react"
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import clamp from 'lodash-es/clamp'
import swap from 'lodash-move'
import { useGesture } from 'react-use-gesture'
import { useSprings, animated, interpolate } from 'react-spring'
import {LectureSettings} from './LectureSettings';
import {css} from "@emotion/core"
import {getCookie} from "./getCookie"
import {ActiveLectureCodes} from "./ActiveLectureCodes";
import {UpdateStudents} from "./UpdateStudents";
import {DataTable} from "./DataTable";
import Modal from 'react-modal';
import axios from "axios";
import '../css/tabs.css'


const tabs = () =>css({
    display:'flex',
    alignItems:'center',
    flexFlow:'column',
})

const icon = () =>css({
    height:30,
    width:30,
    padding:10,
    borderRadius:'50%',
    overflow:'visible',
    backgroundColor:'#f3f3f3',
    fill:'#000000a6',
    margin:'0 10px',
    cursor:'pointer'
})

export default function DashBoard(){
    return(
        <Tabs css={tabs} className="tabs">
            <TabList className="tab-list" css={{display:'flex',width:'100%',justifyContent:'space-around'}}>
                <Tab>
                    Lectures
                </Tab>
                <Tab>
                    Active lecture
                </Tab>
                <Tab>
                    Update students
                </Tab>
                <Tab>
                    All codes
                </Tab>
            </TabList>
            <TabPanel style={{width:'100%'}}>
                <Lectures/>
            </TabPanel>
            <TabPanel style={{width:'100%'}}>
                <ActiveLectureCodes/>
            </TabPanel>
            <TabPanel style={{width:'100%'}}>
                <UpdateStudents/>
            </TabPanel>
            <TabPanel style={{width:'100%'}}>
                <DataTable/>
            </TabPanel>
        </Tabs>
    )
}

function Lectures(){
    const [lectures,setLectures] = useState([]);
    const [activeLecture,setActiveLecture] = useState(null);

    async function getLectures(){
        let response = await axios.get('/api/lectures/');
        let active = response.data.find(l=>l.is_active==true);
        setActiveLecture(active);
        setLectures(response.data);
    }

    useEffect(()=>{
        getLectures();
    },[])

    return(
        <div>
            {lectures.map(l=>{
                return <React.Fragment key={l.id}>
                    <LectureDetails lecture={l} activeLecture={activeLecture} setActiveLecture={setActiveLecture}/>
                </React.Fragment>
            })}
            <CreateNewLecture setLectures={setLectures}/>
        </div>
    )
}

function LectureDetails({lecture,activeLecture,setActiveLecture}){

    const [editing,setEditing] = useState(false);
    const [lectureUrl,setLectureUrl] = useState(null);
    const [showCodes,setShowCodes] = useState(false);
    let isActive = activeLecture?activeLecture.id==lecture.id?true:false:false;

    let activeStyle = isActive?
    {
        backgroundColor:'#4CAF50',
        fill:'white'
    }:null

    async function handleClick(){
        if(isActive){
            setActiveLecture(null);
        }else{
            setActiveLecture(lecture);
        }
        

        let uri = `/api/lectures/${lecture.id}/`
        const config = {
            headers:{
                'X-CSRFToken':getCookie('csrftoken')
            }
        }
        // inform server to mark lecture as active-inactive
        let response = await axios.patch(uri, {
            is_active:isActive?false:true
        },config)

        setLectureUrl(response.data.url);
    }

    return (
    <div style={{padding:10,margin:10,borderRadius:8}} css={{boxShadow:'0px 2px 4px -1px #00000096'}}>
        <div css={{display:'flex',justifyContent:'space-around',alignItems:'center'}}>
            <div css={{margin:'1em',display:'flex',flexFlow:'column'}}>
                <p style={{margin:0}}>{lecture.title}</p>
                <button onClick={()=>setShowCodes(true)}>Codes</button>
            </div>
            
            <div>
                <PencilSvg onClick={()=>setEditing(!editing)}/>
                <TickSvg onClick={handleClick} style={activeStyle}/>
            </div>
            
        </div>
        <Modal isOpen={showCodes} contentLabel="example">
            <button onClick={()=>setShowCodes(false)}>Click me</button>
            <LectureCodes lecture={lecture}/>
        </Modal>
        {editing?<div><LectureSettings lecture={lecture}/></div>:null}
    </div>
    )
}

const grid = () =>css({
    width:'100%',
    height:'100%',
    display:'grid',
    gridTemplateColumns:'repeat(12, minmax(4vmin, 1fr))',
    gridAutoRows:'1fr',
    '@media (max-width:1224px)':{
        gridTemplateColumns:'repeat(6, minmax(4vmin, 1fr))',
    },
    '@media (max-width:767px)':{
        gridTemplateColumns:'repeat(4, minmax(4vmin, 1fr))',
    }
})

function LectureCodes({lecture}){
    const [codes,setCodes] = useState([]);

    async function getCodes(){
        try{
            let response = await axios.get(`/api/lectures/${lecture.id}/codes/`);
            setCodes(response.data);
        }catch(e){

        }
    }

    useEffect(()=>{
        getCodes();
    },[])

    return(
        <div css={grid}>
            {codes.map(c=>{
                let color = null;
                if(c.in_use && c.student){
                    color='blue'
                }else if(c.in_use){
                    color='red'
                }
                return <div css={{display:'flex',justifyContent:'center',alignItems:'center',color:color}}>{c.code_id}</div>
            })}
        </div>
    )
}

function CreateNewLecture({setLectures}){
    return(
        <div style={{padding:10,margin:10,borderRadius:8}} css={{boxShadow:'0px 2px 4px -1px #00000096'}}>
            <div css={{display:'flex',justifyContent:'space-around',alignItems:'center'}}>
                <div css={{margin:'1em',display:'flex',flexFlow:'column'}}>
                    <p style={{margin:0}}>Create new lecture</p>
                </div>
            </div>
            <div><LectureSettings setLectures={setLectures} createNew/></div>
        </div>
    )
}

function Input({lecture}){
    return(
        <div style={{margin:'0.7em'}}>
            <span style={{display:'block',fontSize:'0.7em'}}>{label}</span>
            <input type="text" style={{height:30,borderRadius:'1px solid #c3c3c3',width:'50%',padding:5}} 
            placeholder={lecture.title}/>
        </div>
    )
}


// Returns fitting styles for dragged/idle items
const fn = (order, down, originalIndex, curIndex, y) => index =>
  down && index === originalIndex
    ? { y: curIndex * 100 + y, scale: 1.1, zIndex: '1', shadow: 15, immediate: n => n === 'y' || n === 'zIndex' }
    : { y: order.indexOf(index) * 100, scale: 1, zIndex: '0', shadow: 1, immediate: false }

function DraggableList({ items }) {
  const order = useRef(items.map((_, index) => index)) // Store indicies as a local ref, this represents the item order
  const [springs, setSprings] = useSprings(items.length, fn(order.current)) // Create springs, each corresponds to an item, controlling its transform, scale, etc.
  const bind = useGesture(({ args: [originalIndex], down, delta: [, y] }) => {
    const curIndex = order.current.indexOf(originalIndex)
    const curRow = clamp(Math.round((curIndex * 100 + y) / 100), 0, items.length - 1)
    const newOrder = swap(order.current, curIndex, curRow)
    setSprings(fn(newOrder, down, originalIndex, curIndex, y)) // Feed springs new style data, they'll animate the view without causing a single render
    if (!down) order.current = newOrder
  })
  return (
    <div className="content" style={{ height: items.length * 100 }}>
      {springs.map(({ zIndex, shadow, y, scale }, i) => (
        <animated.div
          {...bind(i)}
          key={i}
          style={{
            zIndex,
            boxShadow: shadow.interpolate(s => `rgba(0, 0, 0, 0.15) 0px ${s}px ${2 * s}px 0px`),
            transform: interpolate([y, scale], (y, s) => `translate3d(0,${y}px,0) scale(${s})`)
          }}
          children={items[i]}
        />
      ))}
    </div>
  )
}

const TickSvg = (props)=>{
    return(
        <svg
        xmlns="http://www.w3.org/2000/svg"
        enableBackground="new 0 0 541.038 541.038"
        viewBox="0 0 541.038 541.038"
        css={icon}
        {...props}
        >
        <path d="m184.405 461.013-184.405-184.405 24.354-24.354 160.051 160.051 332.279-332.279 24.354 24.354z" />
        </svg>
    )
}

const PencilSvg = (props) =>{
    return(
        <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        version="1.1"
        id="Capa_1"
        x="0px"
        y="0px"
        viewBox="0 0 469.331 469.331"
        style={{ enableBackground: "new 0 0 469.331 469.331" }}
        xmlSpace="preserve"
        css={icon}
        {...props}
        >
        <g>
            <path d="M438.931,30.403c-40.4-40.5-106.1-40.5-146.5,0l-268.6,268.5c-2.1,2.1-3.4,4.8-3.8,7.7l-19.9,147.4   c-0.6,4.2,0.9,8.4,3.8,11.3c2.5,2.5,6,4,9.5,4c0.6,0,1.2,0,1.8-0.1l88.8-12c7.4-1,12.6-7.8,11.6-15.2c-1-7.4-7.8-12.6-15.2-11.6   l-71.2,9.6l13.9-102.8l108.2,108.2c2.5,2.5,6,4,9.5,4s7-1.4,9.5-4l268.6-268.5c19.6-19.6,30.4-45.6,30.4-73.3   S458.531,49.903,438.931,30.403z M297.631,63.403l45.1,45.1l-245.1,245.1l-45.1-45.1L297.631,63.403z M160.931,416.803l-44.1-44.1   l245.1-245.1l44.1,44.1L160.931,416.803z M424.831,152.403l-107.9-107.9c13.7-11.3,30.8-17.5,48.8-17.5c20.5,0,39.7,8,54.2,22.4   s22.4,33.7,22.4,54.2C442.331,121.703,436.131,138.703,424.831,152.403z" />
        </g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        </svg>
    )
}