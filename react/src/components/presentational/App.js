import React, {useContext} from "react"
import {Router,Route,Switch} from "react-router-dom";
import history from "./history"
import {css} from "@emotion/core";
import UserCode from './UserCode'
import DashBoard from './DashBoard'
import {Login} from "./Login";
import {UserContext} from "../container/Context";
import { StaffPage } from "./PairCodes";

const container = () =>css({
    display:'flex',
    justifyContent:'center',
    height:'100%',
    width:'100%',
    fontSize:'2em'
})


export default function App(){

    const userContext = useContext(UserContext);

    return(
            <div css={container}>
                <Router history={history}>
                    <Switch>
                        <Route path="/dashboard" component={()=>userContext.isAuth?<DashBoard/>:<Login/>}></Route>
                        <Route path="/pairing" component={StaffPage}></Route>
                        <Route exact path="/" component={UserCode}></Route>
                    </Switch>
                </Router>
            </div>
    )
}

