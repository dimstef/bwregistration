import React, {useState, useEffect} from "react"
import {css} from "@emotion/core";
import axios from "axios";

const center = () =>css({
    display:'flex',
    justifyContent:'center',
    alignItems:'center'
})

const codeWrapper = () =>css({
    flexFlow:'column',
    display:'flex',
    justifyContent:'center',
    alignItems:'center'
})

export default function UserCode(){
    const [code,setCode] = useState(null);
    const [hasMore,setHasMore] = useState(true);

    async function getCode(){
        let response = await axios.get('/api/code/');

        if(response.data.lecture == null){
            setHasMore(false);
        }else{
            setCode(response.data)
        }
    }

    useEffect(()=>{
        getCode()
    },[])

    return(
        <div css={codeWrapper} style={{fontSize:'2em'}}>

            <span>
                Save this code
            </span>

            <span>
                {code?code.code_id:null}
            </span>
        </div>
    )
}