import React, {useEffect,useState,useContext,useRef} from "react"
import {LectureSettings} from './LectureSettings';
import {css} from "@emotion/core"
import {UserContext} from "../container/Context"
import axios from "axios";
import {getCookie} from "./getCookie"

export function ActiveLectureCodes(){
    const [activeLecture,setActiveLecture] = useState(null);
    const [codes,setCodes] = useState([]);
    const [visibleCodes,setVisibleCodes] = useState([]);

    async function getActiveLecture(){
        let response = await axios.get('/api/active_lecture/');
        setActiveLecture(response.data[0])
    }

    async function getCodes(){
        let response = await axios.get(`/api/lectures/${activeLecture.id}/codes/`);
        setCodes(response.data);
    }

    useEffect(()=>{
        getActiveLecture();
    },[])

    useEffect(()=>{
        if(activeLecture){
            getCodes();
        }
    },[activeLecture])

    useEffect(()=>{
        setVisibleCodes(codes);
    },[codes])

    return(
        <div>
            <div css={{display:'flex',flexFlow:'column',justifyContent:'center',alignItems:'center'}}>
                {activeLecture?<p>{activeLecture.title}</p>:null}
                <SearchBox codes={codes} setCodes={setVisibleCodes}/>
            </div>
            <ul css={{backgroundColor:'#f9f9f9',borderRadius:15,padding:'10px 0',listStyleType:'none',fontSize:'1.4rem'}}>
                {visibleCodes.map((c,i)=>{
                    let isLast = i==visibleCodes.length - 1
                    return (
                        <li><Code code={c} isLast={isLast}/></li>
                    )
                })}
            </ul>
        </div>
    )
}


function SearchBox({codes,setCodes}){

    function handleInput(e){
        const filteredCodes = codes.filter(c=>{
            return c.code_id.toString().includes(e.target.value)
        })
        setCodes(filteredCodes);
    }

    return(
        <input onInput={handleInput} type="text" placeholder="Search codes"></input>
    )
}

function Code({code,isLast}){
    const [studentId,setStudentId] = useState(null);
    const [student,setStudent] = useState(code.student);
    let color = 'inherit';

    if(code.in_use && code.student){
        color = 'blue';
    }else if(code.in_use){
        color = '#f44336';
    }

    async function onClick(id=null){
        const config = {
            headers:{
                'X-CSRFToken':getCookie('csrftoken')
            }
        }

        const data = {
            student_id:id || studentId,
        }   

        try{
            let response = await axios.patch(`/api/code/${code.id}/`,data,config)
            setStudent(response.data.student)
        }catch(e){

        }
    }

    function handleInput(e){
        setStudentId(e.target.value);

        if (e.keyCode === 13) {
            onClick(e.target.value);
        }
    }
    
    return(
        <div css={{borderBottom:isLast?null:'1px solid #ffffff',padding:10,
        display:'flex',alignItems:'center',justifyContent:'space-around'}}>
            <div css={{flex:'1 1 auto'}}>
                <div css={{color:color}}>{code.code_id}</div>
                {student?
                <span css={{fontSize:'1rem',wordBreak:'break-word'}}>Used by: {student.full_name}</span>
                :null}
            </div>
            {code.in_use?<div>
                <input type="text" onInput={handleInput} value={student?student.student_id:null} placeholder="Enter student id" 
                css={{padding:10,borderRadius:25,border:'1px solid #b3b3b3'}}/>
                <Save onClick={onClick}/>
            </div>
            :null}
            
        </div>
    )
}

function Save({onClick}){

    function handleClick(){
        onClick();
    }

    return(
        <button onClick={handleClick}
        css={{borderRadius:25,padding:'10px 5px',backgroundColor:'transparent',margin:'0 5px',border:'1px solid #b3b3b3',
        backgroundColor:'white' }}>Save</button>
    )
}