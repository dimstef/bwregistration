import React, {useEffect,useState} from "react"
import axios from "axios"
import {getCookie} from "./getCookie"

export function UpdateStudents(){

    const [file,setFile] = useState(null);

    async function upload(){

        try{
            let formData = new FormData();
            formData.append("file", file);
    

            let config = {
                headers:{
                    'X-CSRFToken':getCookie('csrftoken'),
                    'Content-Type': 'multipart/form-data'
                }
            }

            let response = await axios.post('/api/upload_students/',formData,config)
        }catch(e){
            console.log(e.response)
        }
    }

    function handleClick(){
        upload();
    }

    return(
        <div>
            <input type="file" placeholder="Enter csv file" onInput={e=>setFile(e.target.files[0] || null)}/>
            <UploadButton onClick={handleClick}/>
        </div>

    )
}

function UploadButton({onClick}){
    return(
        <button onClick={onClick}>Submit</button>
    )
}