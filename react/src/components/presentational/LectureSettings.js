import React, {useState,useEffect} from "react"
import {Field,Form} from "react-final-form";
import axios from 'axios';
import {getCookie} from "./getCookie"

export function LectureSettings({lecture,createNew=false,setLectures}){

    async function onSubmit(values){
        const config = {
            headers:{
                'X-CSRFToken':getCookie('csrftoken')
            }
        }

        if(!createNew){
            let uri = `/api/lectures/${lecture.id}/`
            let data = {...values}
            try{
                let response = await axios.patch(uri,data,config)
            }catch(e){
    
            }
        }else{
            let uri = `/api/lectures/`;
            let data = {...values};
            try{
                let response = await axios.post(uri,data,config)
                setLectures(lectures=>[...lectures,response.data])
            }catch(e){
    
            }
        }
        
    }

    return(
        <Form onSubmit={onSubmit} render={({handleSubmit,submitting,submitSucceeded,submitFailed,pristine,
        invalid,errors})=>{
            return(
                <form style={{margin:'0.7em'}} onSubmit={handleSubmit}>
                    <Field name="title" type="text" >{({input,meta})=>(
                        <div>
                            <span style={{display:'block',fontSize:'0.7em'}}>Title</span>
                            <input {...input} placeholder={createNew?'Add a title':lecture.title} type="text" 
                            style={{height:30,border:'1px solid #c3c3c3',borderRadius:24,width:'50%',padding:5}}/>
                            {meta.error && meta.touched && <span>{meta.error}</span>}
                        </div>
                    )}
                    </Field>
                    {submitSucceeded?<p>{createNew?'Successfully created lecture':'Successfully updated lecture'}</p>:null}
                    {submitFailed && errors.length == 0? <p>An error occured</p>:null}
                    <Save submitting={submitting} submitSucceeded={submitSucceeded} pristine={pristine} invalid={invalid}/>
                </form>
            )
        }}></Form>
    )
}

function Save({submitting,pristine,invalid}){
    return(
        <button type="submit" disabled={submitting || pristine || invalid}>
            Save
        </button>
    )
}