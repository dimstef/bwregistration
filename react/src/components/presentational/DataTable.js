import React, {useEffect,useState} from "react"
import axios from "axios"
import {css} from "@emotion/core";
import {getCookie} from "./getCookie"


const row = (color=null) =>css({
    fontSize:'1.3rem',
    margin:'20px 0',
    height:20,
    flexBasis:'25%',
    textAlign:'center',
    color:color
})

const columnName = () =>css({
    fontSize:'2rem',
    margin:'20px 0',
    flexBasis:'25%',
    textAlign:'center'
})


export function DataTable(){

    const [codes,setCodes] = useState([]);

    async function getData(){
        try{
            let response = await axios.get('/api/all_codes/');
            setCodes(response.data);
        }catch(e){

        }
    }

    useEffect(()=>{
        getData();
    },[])

    return(
        <div css={{width:'100%',display:'flex',flexFlow:'column',justifyContent:'space-around',backgroundColor:'#f9f9f9',
        borderRadius:25,padding:20}}>
            {/*<div css={{display:'flex',flexFlow:'column'}}>
                <span>Lecture</span>
                {codes.map(c=>{
                    return <p css={row}>{c.lecture}</p>
                })}
            </div>
            <div css={{display:'flex',flexFlow:'column'}}>
                <span>Codes</span>
                {codes.map(c=>{
                    return <p css={row}>{c.code_id}</p>
                })}
            </div>
            <div css={{display:'flex',flexFlow:'column'}}>
                <span>Used by</span>
                {codes.map(c=>{
                    return <p css={row}>{c.student?c.student.full_name:null}</p>
                })}
            </div>
            <div css={{display:'flex',flexFlow:'column'}}>
                <span>Checked by</span>
                {codes.map(c=>{
                    return <p css={row}>{c.checked_by?c.checked_by:null}</p>
                })}
            </div>*/}
            <div css={{display:'flex',justifyContent:'space-around'}}>
                <p css={columnName}>Lectures</p>
                <p css={columnName}>Codes</p>
                <p css={columnName}>Used by</p>
                <p css={columnName}>Checked by</p>
            </div>
            {codes.map(c=>{
                let color = null;
                if(c.in_use && c.student){
                    color = 'blue';
                }else if(c.in_use){
                    color = 'red';
                }

                return <div key={c.id} css={{display:'flex',justifyContent:'space-around'}}>
                    <p css={row}>{c.lecture}</p>
                    <p css={()=>row(color)}>{c.code_id}</p>
                    <p css={row}>{c.student?c.student.full_name:null}</p>
                    <p css={row}>{c.checked_by?c.checked_by:null}</p>
                </div>
            })}
        </div>
    )
}