import React, {useContext,useState,useEffect,useLayoutEffect} from "react";
import {UserContext} from "../container/Context";
import {Login} from "./Login"
import {Field,Form} from "react-final-form";
import { FORM_ERROR } from 'final-form'
import history from "./history"
import axios from 'axios';
import css from "@emotion/css";
import {getCookie} from "./getCookie";

export function StaffPage(){
    const userContext = useContext(UserContext);

    // check first if context is already populated with "user"
    const [isStaff,setStaff] = useState(userContext.user?userContext.user.is_staff:false)

    async function getUser(){
        try{
            let response = await axios.get('/api/user/');
            setStaff(response.data[0].is_staff)
        }catch(e){
            
            console.log(e.response)
        }
    }

    function handleResponse(r){
        getUser();
    }

    useLayoutEffect(()=>{
        // if user is authenticated but has not received the "user" property fetch it
        // this would happen if the user logged in then refreshed the page
        if(userContext.isAuth){
            getUser();
        }
    },[])

    return (
        userContext.isAuth?
            isStaff?<CodePairer/>:<p>You don't have access to this page</p>
        :<Login handleResponse={handleResponse}/>
    )
}


const inputCss = () =>css({
    display:'block',
    padding:10,
    border:'1px solid #c7c7c7',
    borderRadius:25
})

const form = () =>css({
    width:'100%',
    height:'fit-content',
    display:'flex',
    justifyContent:'space-evenly',
    alignItems:'center',
    margin:'30px 0',
    padding:20,
    boxSizing:'border-box',
    borderRadius:8,
    boxShadow:'0px 2px 4px -1px #00000096'
    
})

const container = () =>css({
    width:'40%',
    '@media (max-device-width: 767px)':{
        width:'90%'
    }
})
function CodePairer(){

    async function onSubmit(values){
        const config = {
            headers:{
                'X-CSRFToken':getCookie('csrftoken')
            }
        }

        const data = {
            student_id:values.student_id,
        }   

        try{
            let response = await axios.patch(`/api/code/${values.code}/`,data,config)
        }catch(e){
            return {[FORM_ERROR]: "an error occured"}

        }
    }

    return(
        <div css={container}>
            <Form onSubmit={onSubmit} render={({handleSubmit,submitting,submitSucceeded,submitFailed,pristine,
            invalid,errors,submitError})=>{
                return(
                    <>
                    <form css={form} onSubmit={handleSubmit}>
                        <Field name="code" type="text" >{({input,meta})=>(
                            <div>
                                <label>Code</label>
                                <input {...input} css={inputCss} required/>
                                {meta.error && meta.touched && <span>{meta.error}</span>}
                            </div>
                        )}
                        </Field>
                        <Field name="student_id" type="text" >{({input,meta})=>(
                            <div>
                                <label>Student Id</label>
                                <input {...input} css={inputCss} required/>
                                {meta.error && meta.touched && <span>{meta.error}</span>}
                            </div>
                        )}
                        </Field>
                        <Pair submitting={submitting} submitSucceeded={submitSucceeded} pristine={pristine} invalid={invalid}/>
                    </form>
                    {submitError?<div css={{display:'flex',justifyContent:'center',margin:'30px 0'}}><XSvg/></div>:null}
                    {submitSucceeded?<TickSvg/>:null}
                    </>
                )
            }}></Form>
        </div>
    )
}

function Pair(){
    return (
        <button type="submit" style={{height:30}}>Pair</button>
    )
}


const icon = () =>css({
    height:24,
    width:24
})

const TickSvg = (props)=>{
    return(
        <svg
        xmlns="http://www.w3.org/2000/svg"
        enableBackground="new 0 0 541.038 541.038"
        viewBox="0 0 541.038 541.038"
        css={icon}
        style={{fill:'green'}}
        {...props}
        >
        <path d="m184.405 461.013-184.405-184.405 24.354-24.354 160.051 160.051 332.279-332.279 24.354 24.354z" />
        </svg>
    )
}

const XSvg = (props) =>{
    return(
        <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        version="1.1"
        viewBox="0 0 28 28"
        style={{ enableBackground: "new 0 0 28 28" }}
        xmlSpace="preserve"
        css={icon}
        style={{fill:'red'}}

        >
        <g>
            <g>
            <g>
                <polygon
                points="28,22.398 19.594,14 28,5.602 22.398,0 14,8.402 5.598,0 0,5.602 8.398,14 0,22.398      5.598,28 14,19.598 22.398,28    "
                />
            </g>
            </g>
        </g>
        </svg>

    )
}