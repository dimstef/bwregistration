import React from "react"

export const UserContext = React.createContext({
    isAuth:localStorage.getItem('token') || false
});