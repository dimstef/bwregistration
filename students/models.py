from django.db import models


class Student(models.Model):
    student_id = models.PositiveIntegerField()
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    email = models.EmailField()

    @property
    def full_name(self):
        return '%s %s' % (self.name, self.surname)

    def __str__(self):
        return '%s %s' % (self.name, self.surname)
